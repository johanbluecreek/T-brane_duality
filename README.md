# The two faces of T-branes -- Supplement

This notebook contains supplementary details to the article "The two faces of T-branes", by Iosif Bena, Johan Blåbäck, Raffaele Savelli, and Gianluca Zoccarato, [1905.03267](https://arxiv.org/abs/1905.03267).

This notebook as a slightly different outline compared to the paper. In this notebook, the non-Abelian and Abelian systems are considered separately. The expressions are derived for four supercharges, and then appropriately truncated to the expressions for eight supercharges, to be compared with the expressions in the article. The DBI and WZ actions are considered in that order.

For the reader's convenience, we have added comments with the phrase "compare to", which would point towards the appropriate equation in the article to which that expression should be compared. Hence a search for the phrase "compare to" in this notebook would bring up all instances where expressions can be compared to the paper directly.

In making a comparison, the reader should be aware of what the expressions in this notebook represents. Expressions in this notebook are often written as Lagrangian densities, rather than complete actions, traces are not performed or even present, and for the Abelian side, there is not always a sum over the strands/sheets. This means that overall factors (such as N, the Jacobian) and operations (such as sums, integrals, and traces) may be absent in this notebook compared to the article. Ordering of indices also differ between notebook and article, which can easily be mistaken for a sign error, so we ask the reader to keep this in mind.

Unfortunately this is a Mathematica notebook, and because of this we have provided a pdf version of the notebook that can be read independently of Mathematica.

## Dependencies

This notebook depends on the package `diffgeo.m` by M. Headrick, which at the time of writing is hosted on

[http://people.brandeis.edu/~headrick/Mathematica/](http://people.brandeis.edu/~headrick/Mathematica/)

which has been archived on the Internet Archive Wayback Machine

[https://web.archive.org/web/20190301000000*/http://people.brandeis.edu/~headrick/Mathematica/diffgeo.m](https://web.archive.org/web/20190301000000*/http://people.brandeis.edu/~headrick/Mathematica/diffgeo.m)

This package is not distributed together with this notebook, and paths in this notebook that point to this package needs to be changed in order for the notebook to be able to execute commands the way they are intended.

## Caution

Words of caution: This notebook redefines expressions in going from calculation on the non-Abelian side to the Abelian side, hence care should be taken when working with this file. Best is to only run this file using "Evaluate Notebook".

## License

This is free and unencumbered software released into the public domain. See `LICENSE` file for details.
